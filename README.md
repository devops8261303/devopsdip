Для создания инфрструктуры использовались terraform и yandex cloud.
Для установки базового прораммоного обеспечения использовался ansible
Установка кластера производилась посредством подключения к ВМ по SSH. 
Информация о соединениях, ip адресах, есть в файе Network Diagram.png
SSH ключи добавил с помощью метаданных передаваемых в конфигурации terraform
Кластер устанавливал используя оф. сайт лог комманд приведён ниже 

sudo apt-get update
# apt-transport-https may be a dummy package; if so, you can skip that package

sudo apt-get install -y apt-transport-https ca-certificates curl gpg
# If the directory `/etc/apt/keyrings` does not exist, it should be created before the curl command, read the note below.
# sudo mkdir -p -m 755 /etc/apt/keyrings

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list

echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update

sudo apt-get install -y kubelet kubeadm kubectl

sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl enable --now kubelet

Настройки сети были заданы следующей командой 

Сервер k8s master

iptables -I INPUT 1 -p tcp --match multiport --dports 6443,2379:2380,10250:10252 -j ACCEPT

Сервер k8sapp

iptables -I INPUT 1 -p tcp --match multiport --dports 10250,30000:32767 -j ACCEPT

Инициализация кластера:

kubeadm init --pod-network-cidr=10.244.0.0/16